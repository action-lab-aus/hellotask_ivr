const trunkDetails = require('./keys/siptrunk.json');
const xml2js = require('xml2js');
const esl = require('modesl');

// Dialplan destinations
const dialplan_reject = 69421;

const Event = {
    Connection: {
        READY: 'esl::ready',
        CLOSED: 'esl::end',
        ERROR: 'error',
    },
    RECEIVED: 'esl::event::*::*',
    DialResults: {
        OK : '+OK',
        REJECTED : 'CALL_REJECTED',
        NO_USER: 'USER_NOT_REGISTERED',
        BUSY: 'USER_BUSY',
        NO_ANSWER : 'NO_ANSWER',
        NO_RESPONSE : 'NO_USER_RESPONSE'
    }
};

const subbedEvents = ['CHANNEL_HANGUP'];

var lastParked = 1000;
var callData = {};
var onCallEnd = {};

class CallManager 
{
    constructor(eventHandler)
    {
        this.eventHandler = eventHandler
        this.processedEvents = [];

        // initialise inbound ESL connection to start listening for events
        this.eslExecute('log NOTICE Starting inbound ESL')

        // handle incoming calls with an outbound ESL connection
        this.eslServer = new esl.Server({ port: 8040, myevents: true }, () => {
          console.log('esl outbound server is up');
        }); 
        this.eslServer.on('connection::ready', async (conn, id) => {
            var info = conn.getInfo();
            console.log('connection ready')
            // console.log(info.serialize());
            if(info.getHeader('Channel-Direction') === 'outbound')
            {
                const uuid = info.getHeader('Unique-ID');
                if(callData[uuid] && !callData[uuid].handled)
                {
                    console.log(callData[uuid])
                    callData[uuid].handled = true;
                    await callData[uuid]['callback'](callData[uuid].data, {'menu' : callData[uuid].data.start}, conn, uuid);
                }
            }
            else
            {
                console.log("Inbound call!")
                console.log(info.serialize());

                conn.execute('transfer', dialplan_reject, resp => {
                    console.log("Transferred to rejection")
                    conn.disconnect();
                });
            }
        }); 
    }

    eslConnect()
    {
        return new Promise((resolve, reject) =>
        {
            try
            {
                if(!this.eslConn)
                {
                    console.log("Connecting...")
                    this.eslConn = new esl.Connection('::1', '8021', 'ClueCon');
                    this.eslConn.on(Event.Connection.ERROR, (err) => {
                        // Error connecting to FreeSWITCH!
                        console.error(err);
                        reject('Connection error');
                    });
                    this.eslConn.on(Event.Connection.CLOSED, () => {
                        // Connection to FreeSWITCH closed!
                        reject('Connection closed');
                    });
                    this.eslConn.on(Event.Connection.READY, () => {
                        // Connection to FreeSWITCH established!
                        console.log("connection established")
                        this.eslConn.events('json', subbedEvents);
                        resolve(this.eslConn);
                    });                    
                    this.eslConn.on(Event.RECEIVED, (event) => {
                        const name = event.getHeader('Event-Name');

                        if(name === 'CHANNEL_HANGUP')
                        {
                            let uuid = event.getHeader('Unique-ID');
                            console.log(uuid + ' just hung up')
                            
                            // check if anyone is interested in this call ending
                            let onEnd = onCallEnd[uuid]
                            if(onEnd)
                            {
                                onEnd.cb(onEnd.conn, onEnd.data)
                            }
                        }
                    });
                }
                else
                {
                    resolve(this.eslConn);
                }
            }
            catch (error) 
            {
                console.log(error)
                reject(error)
            }
        });        
    }

    eslExecute(command)
    {
        return new Promise((resolve, reject) => 
        {
            this.eslConnect()
            .then(connection => {
                connection.bgapi(command, response => {
                    resolve(response);
                });
            })
            .catch(error => {
                reject(error);
            });
        });
    } 

    async makeCall(recipient, data, callback, retries)
    {
        retries--;
        try 
        {
            let uuid_res = await this.eslExecute('create_uuid')
            let uuid = uuid_res.getBody();

            callData[uuid] = { "data" : data, "callback" : callback, "handled" : false };

            // initialise call using our uuid so we can track it
            let originateArgs = `origination_uuid=${uuid},origination_caller_id_number=${trunkDetails.callernumber},ignore_early_media=true,execute_on_answer=start_dtmf`;
            let res = await this.eslExecute(`originate {${originateArgs}}sofia/gateway/${trunkDetails.gateway}/${recipient} &socket(127.0.0.1:8040 async full)`);
            console.log("make call res")
            console.log(res)
            return {'uuid' : uuid, 'res' : res};
        } 
        catch (error) 
        {
            console.log(error)
            if(retries > 0)
            {
                // wait 2 secs before retrying
                await new Promise(r => setTimeout(r, 2000));
                return this.makeCall(recipient, data, callback, retries)
            }
            else throw error;
        }                
    }

    async parkAndCall(conn, numToCall, onFailure, onSuccess, sessionData)
    {
        sessionData.parkingLot = lastParked++;

        // park the current call, start a new one and connect the two once the new person answers
        conn.execute('valet_park', `hellotask ${sessionData.parkingLot}`);

        try 
        {
            let call = await this.makeCall(numToCall, sessionData, onSuccess, 2)
            if(!call || !call.res || !call.res.body.startsWith('+OK'))
            {
                console.log(call)
                onFailure(conn, sessionData);
            }
            
            onCallEnd[call.uuid] = {
                'conn' : conn,
                'cb' : onFailure,
                'data' : sessionData
            }

            return sessionData.parkingLot;
        }
        catch (error) 
        {
            console.log(error)
            onFailure(conn, sessionData);
            return null;
        }
    }

    async checkAndConnect(conn, data)
    {
        // check that the original caller is still in the parking lot
        let lotInfo = await this.eslExecute('valet_info hellotask');

        let xmlval = await xml2js.parseStringPromise(lotInfo.getBody());

        if(!xmlval.lots || !xmlval.lots.lot || xmlval.lots.lot.length === 0) 
        {
            console.log("No one parked");
            stopWaitingForCallEnd(conn);
            return false;
        }

        let lotFound = false;
        for(let lot of xmlval.lots.lot)
        {
            if(lot.extension && lot.extension[0] && parseInt(lot.extension[0]['_']) === data.parkingLot)
            {
                lotFound = true;
                break;
            }
        }

        // if they are, transfer this call into the lot and return true
        if(lotFound)
        {
            await conn.execute('valet_park', `hellotask ${data.parkingLot}`);

            // we're also done with the person who was already parked
            this.stopWaitingForCallEnd(conn);
            conn.disconnect();
            return true;
        }
        // otherwise, return false
        return false;
    }

    stopWaitingForCallEnd(conn)
    {
        const info = conn.getInfo();
        const thisUuid = info.getHeader('Unique-ID');

        if(onCallEnd[thisUuid])
        {
            if(onCallEnd[thisUuid].conn)
            {
                onCallEnd[thisUuid].conn.disconnect();
            }
            delete onCallEnd[thisUuid];
        }

    }

    async endParkedCallWithMessage(conn, audioFiles)
    {     
        await conn.execute('set', `hellotask_message=${audioFiles.join('!')}`);
        await conn.execute('transfer', 6942)
    }

    async disconnectCall(uuid)
    {
        return await this.eslExecute(`uuid_kill ${uuid}`);
    }
}

module.exports = CallManager;