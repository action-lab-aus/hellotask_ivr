const CallManager = require('./call_manager.js');
const MenuManager = require('./menu_manager.js');
const FileSystemManager = require('./filesystem_manager.js');
var express = require('express');
var app = express();
app.use(express.json());

var server = app.listen(8675, function () {
    var host = server.address().address
    var port = server.address().port
 })

function eventHandler(event)
{
    //console.log(event);
}

const fsManager = new FileSystemManager();
const menuManager = new MenuManager();
const callManager = new CallManager(eventHandler);

// body format:
// {
//     "call" : "+6133523565",
//     "data" : {
//         "lang" : 'en'
//         "hours" : 3,
//         "area" : "Outset Island",
//         "pay" : 245,
//         "client_name" : "Rose",
//         "address_description" : "https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_700KB.mp3",
//         "repeat_client" : true,
//         "result_webhook" : "https://example.com/api/newworkrequest-call-post",
//         "session_id" : "example_session_id"
//       } 
// }
app.post('/newworkrequest', async function(req, res) {
    // convert bool to int, as it's used as an array index ref in the menu
    req.data.repeat_client = req.data.repeat_client ? 1 : 0; 
    let callFlow = await menuManager.getNewWorkRequestMenus(req.body.data);
    callManager.makeCall(req.body.call, {"reqData" : req.body.data, "menus" : callFlow.menus, "start" : callFlow.start}, newWorkRequest, 3);
    res.status(200).send({ message: 'ok' });
});

// TODO handle early hang-up
async function newWorkRequest(sessionData, currentRoute, conn, uuid)
{
    if(currentRoute.menu === 'Complete')
    {
        var eta = currentRoute.data.ETA;
        sessionData.reqData['minutes_index'] = eta == 30 ? 0 : eta == 60 ? 1 : 2;
        sessionData.menus.Complete = await menuManager.getNewWorkRequestCompleteMenu(sessionData.reqData);

        // TODO tell hellotask success
        console.log("JOB ACCEPTED")
    }
    else if(currentRoute.menu === 'Rejected')
    {
        // TODO tell hellotask failure
        console.log("JOB REJECTED")
    }
    
    const endOfCall = currentRoute.data && currentRoute.data.final;
    menuManager.playMenu(sessionData.menus[currentRoute.menu], uuid, conn, "NewWorkRequest",
         currentRoute.menu, sessionData, endOfCall ? 1 : 3, endOfCall, newWorkRequest);
}

// body format:
// {
//     "call" : "+6133523565",
//     "data" : {
//         "lang" : 'en'
//         "confirmed" : true, 
//         "area" : "Outset Island",
//         "client_name" : "Rose",
//         "address_description" : "https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_700KB.mp3",
//         "client_number" : "+6133523565",
//         "office_number" : "+6133523565",
//         "result_webhook" : "https://example.com/api/confirmation-call-post",
//         "session_id" : "example_session_id"
//       } 
// }
app.post('/confirmationcall', async function(req, res) {
    let callFlow = await menuManager.getCallMenu('ConfirmationCall', req.body.data, req.body.data.confirmed? 'ClientConfirmed' : 'ClientCancelled');
    callManager.makeCall(req.body.call, {
        "reqData" : req.body.data,
        "menus" : callFlow.menus, 
        "start" : callFlow.start
    }, confirmationCall, 3);
    res.status(200).send({ message: 'ok' });
});

// TODO handle early hang-up
async function confirmationCall(sessionData, currentRoute, conn, uuid)
{
    if(currentRoute.menu === 'ContactUser' || currentRoute.menu === 'ContactOffice')
    {
        menuManager.playAudio(sessionData.menus[currentRoute.menu], conn);
        let numToCall = currentRoute.menu === 'ContactUser' ? sessionData.reqData.client_number : sessionData.reqData.office_number;
        sessionData.start = currentRoute.menu;
        callManager.parkAndCall(conn, numToCall, failedToConnect, connectingCallAnswered, sessionData)       
    }
    else
    {
        const endOfCall = currentRoute.menu === 'ClientCancelled' || (currentRoute.data && currentRoute.data.final);
        menuManager.playMenu(sessionData.menus[currentRoute.menu],
            uuid, conn, "ConfirmationCall", currentRoute.menu, sessionData, endOfCall ? 1 : 3, endOfCall, confirmationCall);
    }
}


// body format:
// {
//     "call" : "+6133523565",
//     "data" : {
//         "lang" : 'en'
//         "short_eta" : 15,
//         "longer_eta" : 30,
//         "office_number" : "+6133523565",
//         "result_webhook" : "https://example.com/api/tracking-call-post",
//         "session_id" : "example_session_id"
//       } 
// }
app.post('/trackingcall', async function(req, res) {
    let callFlow = await menuManager.getCallMenu('TrackingCall', req.body.data);
    callManager.makeCall(req.body.call, {
        "reqData" : req.body.data,
        "menus" : callFlow.menus, 
        "start" : callFlow.start
    }, trackingCall, 3);
    res.status(200).send({ message: 'ok' });
});

// TODO handle early hang-up
async function trackingCall(sessionData, currentRoute, conn, uuid)
{
    if(currentRoute.menu === 'ContactOffice')
    {
        menuManager.playAudio(sessionData.menus[currentRoute.menu], conn);
        sessionData.start = currentRoute.menu;
        callManager.parkAndCall(conn, sessionData.reqData.office_number, failedToConnect, connectingCallAnswered, sessionData);
        // end of ivr logic
        return;
    }

    if(currentRoute.menu === 'Complete')
    {
        var eta = currentRoute.data.ETA;
        console.log("eta given: " + eta)
        // TODO inform HelloTask of new eta
    }
    else if(currentRoute.menu === 'RejectConfirmed')
    {
        // TODO tell hellotask failure
        console.log("JOB REJECTED")
    }
    
    const endOfCall = currentRoute.data && currentRoute.data.final;
    menuManager.playMenu(sessionData.menus[currentRoute.menu], uuid, conn, "TrackingCall",
         currentRoute.menu, sessionData, endOfCall ? 1 : 3, endOfCall, trackingCall);
}

// CALL BRIDGING **************************************************************************************************
async function failedToConnect(conn, sessionData)
{
    console.log("failed to connect")
    // TODO let hellotask know
    let callFlow = await menuManager.getCallMenu('CallBridging', sessionData.reqData);
    callManager.endParkedCallWithMessage(conn, callFlow.menus['Failed'])
}

async function connectingCallAnswered(sessionData, currentRoute, conn, uuid)
{
    let callFlow = await menuManager.getCallMenu('CallBridging', sessionData.reqData);

    if(currentRoute.menu === 'ConnectToParked')
    {
        // recipient has said they're happy to connect to caller
        menuManager.playMultipleAudios(callFlow.menus['ConnectToParked'], conn);
        if(await callManager.checkAndConnect(conn, sessionData)) 
        {
            // calls are connected, end of logic
            console.log("calls connected")
            return;
        }
    
        // looks like the initial caller hung up before the called one connected
        return menuManager.playMenu(callFlow.menus['ParkedLeft'], uuid, conn, 'CallBridging', 'ParkedLeft', null, 1, true, null);
    }

    let menu = 'Generic_OnAnswered';
    switch(currentRoute.menu)
    {
        case 'ContactUser':
            menu = 'WorkerToClient_OnClientAnswer';
            break;
        case 'ContactOffice':
            menu = 'WorkerToOffice_OnOfficeAnswer';
            break;            
    }

    menuManager.playMenu(callFlow.menus[menu], uuid, conn, 'CallBridging', menu, sessionData, 2, false, connectingCallAnswered);
}