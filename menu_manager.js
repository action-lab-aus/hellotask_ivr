const textToSpeech = require('@google-cloud/text-to-speech');
var crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const util = require('util');
const ivrJson = require('./IVR_prompts.json');
var appRoot = require('app-root-path');

const ttsClient = new textToSpeech.TextToSpeechClient({
    keyFile: './keys/gcloud.json',
    scopes : ['https://www.googleapis.com/auth/cloud-platform']
});

var callData = {};

class MenuManager 
{
    constructor()
    {
        this.createDirectories('./audio/')
        this.prepAllFiles();
    }

    createDirectories(pathname) {
        return new Promise((resolve, reject) =>
        {
            fs.mkdir(path.resolve(__dirname, pathname), { recursive: true }, err => 
            {
                if (err) 
                {
                    if (err.code == 'EEXIST')
                    {
                        resolve();
                    }
                    console.error(err);
                    reject();
                } 
                else 
                {
                    console.log('Created ' + pathname);
                    resolve();
                }
            });
        });
    }

    // preload as many recordings as we can
    // some rely on dynamic data so they can't be prepared
    async prepAllFiles()
    {
        var promises = [];
        Object.keys(ivrJson).forEach(async callType => {
            Object.keys(ivrJson[callType].Menus).forEach(async menu => {
                Object.keys(ivrJson[callType].Menus[menu].Phrases).forEach(async lang => {
                    promises.push(this.getPhrasesForIvrMenu(lang, callType, menu, null));
                });
                
            });
        });
    }

    async getTTS(text, lang)
    {
        var hash = crypto.createHash('md5').update(text).digest('hex');
        var file = `${appRoot.path}/audio/${hash}.wav`;

        try {
            await fs.promises.access(file);
            // We already have the file
            return file;
        } catch (error) {
            // The check failed, download the file
            console.log("Getting new tts")
        }

        try 
        {
            // Default to Bengali, list other supported languages manually
            var langCode = 'bn-IN';
            if(lang)
            {
                if(lang.toLowerCase() === 'en') langCode = 'en-US'
            }

            const request = {
                input: {'text': text},
                voice: {'languageCode': langCode, 'ssmlGender': 'FEMALE'},
                audioConfig: {'audioEncoding': 'LINEAR16'},
            };

            const [response] = await ttsClient.synthesizeSpeech(request);
            const writeFile = util.promisify(fs.writeFile);
            await writeFile(file, response.audioContent, 'binary');
            return file;
        } 
        catch (error) 
        {
            console.error(error)
        }

    }

    async getPromptRecording(recordingUrl, lang)
    {
        return null; //TODO
    }

    async processIvrPrompt(lang, promptJson, jobData)
    {
        switch(promptJson.phrase_type)
        {
            case 'condition':
                if(!jobData) break;
                return await this.processIvrPrompt(
                    lang,
                    promptJson.options[jobData[promptJson.phrase_index]],
                    jobData);        
            case 'var_tts' :
                if(!jobData) break;
                return this.getTTS(String(jobData[promptJson.phrase_value]), lang);
            case 'var_recording':
                if(!jobData) break;
                return this.getTTS("PLACEHOLDER FOR A DYNAMIC RECORDING DOWNLOADED FROM HELLO TASK", lang); //TODO
            case 'static':
                if(promptJson.recording_url)
                {
                    var localCopy = await this.getPromptRecording(promptJson.recording_url, lang)
                    if(localCopy) return localCopy;
                }
            // if we fail to get a local copy of the recording, resort to TTS
            default:
                return await this.getTTS(promptJson.phrase_value, lang); 
        }

        // should only get here if we're preloading audio
        if(promptJson.phrase_type === 'condition')
        {
            promptJson.options.forEach(async opt => {
                await this.processIvrPrompt(lang, opt, null);    
            })
        }
    }

    async getPhrasesForIvrMenu(lang, callFlow, menuTitle, jobData)
    {
        try 
        {
            var phrases = ivrJson[callFlow].Menus[menuTitle].Phrases[lang];
            var promises = [];
    
            phrases.forEach(phraseJson => {
                promises.push(this.processIvrPrompt(lang, phraseJson, jobData))
            });

            return await Promise.all(promises);    
        } 
        catch (error) 
        {
            console.error(error);
            return [];    
        }
    }

    async getNewWorkRequestMenus(data)
    {
        var toRet = { 'start' : ivrJson['NewWorkRequest'].Start, 'menus' : {} }
        var promises = [];

        promises.push(toRet.menus['InitialOffer'] = await this.getPhrasesForIvrMenu(data.lang, 'NewWorkRequest', 'InitialOffer', data));
        promises.push(toRet.menus['Rejected'] = await this.getPhrasesForIvrMenu(data.lang, 'NewWorkRequest', 'Rejected', data));
        promises.push(toRet.menus['GiveDetailedAddress'] = await this.getPhrasesForIvrMenu(data.lang, 'NewWorkRequest', 'GiveDetailedAddress', data));
        promises.push(toRet.menus['GetETA'] = await this.getPhrasesForIvrMenu(data.lang, 'NewWorkRequest', 'GetETA', data));

        await Promise.all(promises);

        return toRet;
    }

    async getNewWorkRequestCompleteMenu(data)
    {
        return await this.getPhrasesForIvrMenu(data.lang, 'NewWorkRequest', 'Complete', data);
    }

    async getCallMenu(callFlow, data, start = null)
    {
        var toRet = { 'start' : start ? start : ivrJson[callFlow].Start, 'menus' : {} }
        var promises = [];
    
        Object.keys(ivrJson[callFlow].Menus).forEach(async menu => {
            promises.push(new Promise((resolve, reject) => {
                this.getPhrasesForIvrMenu(data.lang, callFlow, menu, data).then(res => {
                    toRet.menus[menu] = res; 
                    resolve();
                });
            }));
        });
    
        await Promise.all(promises);
    
        return toRet;
    }

    playMenu(audioFiles, uuid, conn, root, menu, data, timesPlayed, finalMenu, cb, errcb = null)
    {
        try 
        {
            // if there are no options, don't require a DTMF input
            const minDtmf = finalMenu || !ivrJson[root].Menus[menu].Options || Object.keys(ivrJson[root].Menus[menu].Options).length === 0 ? 0 : 1;
            conn.execute('play_and_get_digits', `${minDtmf} 1 ${timesPlayed} ${finalMenu ? 1000 : 5000} # file_string://${audioFiles.join('!')} /invalid.wav hellotask_dtmf`);

            // If we're not watching this call for dtmf, start doing so
            if(uuid && !callData[uuid])
            {
                conn.execute('start_dtmf');
                conn.on('esl::event::DTMF::*', async (evt) => {
                    const dtmfDigit = evt.getHeader('DTMF-Digit');
                    const thisUuid = evt.getHeader('Unique-ID');
                
                    if(callData[thisUuid] && callData[thisUuid].options[dtmfDigit])
                    {
                        // a menu option for this digit exists
                        callData[thisUuid].cb(callData[thisUuid].sessionData, callData[thisUuid].options[dtmfDigit], conn, thisUuid)
                    }
                    else if(callData[thisUuid] && callData[thisUuid].options['ANY'])
                    {
                        // any digit is allowed
                        callData[thisUuid].cb(callData[thisUuid].sessionData, callData[thisUuid].options['ANY'], conn, thisUuid)
                    }
                    else
                    {
                        console.log(`no dtmf option for ${dtmfDigit} set ${thisUuid}`)
                    }
                });
            }

            if(finalMenu)
            {
                if(uuid) delete callData[uuid];
                conn.disconnect();
            }
            else if(uuid)
            {
                // otherwise, update the available menu options
                callData[uuid] = {
                    'options' : ivrJson[root].Menus[menu].Options,
                    'cb' : cb,
                    'sessionData' : data
                }
            }
        } 
        catch (error) 
        {
            // likely that the call hung up during the menu playback/dtmf listening
            console.error(error)
            if(errcb) errcb(data)
        }
    }

    playAudio(audioFile, conn)
    {
        console.log(`Playing ${audioFile}`)
        conn.execute('playback', audioFile);
    }

    playMultipleAudios(audioFiles, conn)
    {
        conn.execute('playback', `file_string://${audioFiles.join('!')}`);
    }

}

module.exports = MenuManager;