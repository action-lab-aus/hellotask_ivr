const process = require('process');
const { exec } = require("child_process");

class FileSystem_Manager 
{
    constructor()
    {
        // TODO should also set up freeswitch environment
        this.runUserGroupCheck();
    }

    // Check if we're running as a user in the freeswitch group
    runUserGroupCheck()
    {
        exec("id -g freeswitch", (error, stdout, stderr) => {
            if (error) {
                console.error(`error getting freeswitch group info: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr getting freeswitch group info: ${stderr}`);
                return;
            }

            if (process.getgid) {                  
            var gid = process.getgid();
            if(gid == stdout)
            {
                console.log("Running as the freeswitch group, gid", gid)
            }
            else
            {
                console.warn("Not being run in the 'freeswitch' user group. May encounter permission issues with recordings!")
            }
            }
            else
            {
            console.warn("OS doesn't support programmatic checking of usergroup. Make sure this is being run as a member of the 'freeswitch' group!")
            }
        });
    }
}

module.exports = FileSystem_Manager;